function searchUsers(argument) {
    return fetch('/books/search?argument=' + argument)
        .then((response) => {
            return response.json()
        }).then((books) => {
            fillTable(books)
        })
}

function fillTable(books) {
    let table = document.getElementById("booksTable");

    table.innerHTML = '    <tr>\n' +
        '        <th>id</th>\n' +
        '        <th>Name</th>\n' +
        '        <th>Author</th>\n' +
        '    </tr>';

    for (let i = 0; i < books.length; i++) {
        let row = table.insertRow(-1);
        let idCell = row.insertCell(0);
        let nameCell = row.insertCell(1);
        let authorCell = row.insertCell(2);

        idCell.innerHTML = books[i].id;
        nameCell.innerHTML = books[i].name;
        authorCell.innerHTML = books[i].author;
    }
}


function addBook(nameOfBook, author, publisher, color, pages) {
    let body = {
        "name": nameOfBook,
        "author": author,
        "publisher": publisher,
        "color": color,
        "numberOfPages": pages
    };
    fetch("/books", {
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(body)
    }).then((response) => response.json())
        .then((books) => fillTable(books))
        .catch((error) =>
            alert(error))
}


function getOrderColumn() {
    return document.cookie
        .split('; ')
        .find((row) => row.startsWith("orderColumn="))?.split('=')[1];
}


function getListOfBooksByArgument(from, to) {
    return fetch('/app/find/?from=' + from + '&to=' + to + '')
        .then((response) => {
            return response.json()
        }).then((allBooks) => {
            fillAllTable(allBooks)
        })
}


function fillAllTable(allBooks) {
    let table = document.getElementById("mainBooksTable");

    table.innerHTML = '    <tr>\n' +
        '        <th>id</th>\n' +
        '        <th>Name</th>\n' +
        '        <th>Author</th>\n' +
        '        <th>Publisher</th>\n' +
        '        <th>Color</th>\n' +
        // '        <th>Pages</th>\n' +
        '    </tr>';

    for (let i = 0; i < allBooks.length; i++) {
        let row = table.insertRow(-1);
        let idCell = row.insertCell(0);
        let nameCell = row.insertCell(1);
        let authorCell = row.insertCell(2);
        let publisherCell = row.insertCell(3);
        let colorCell = row.insertCell(4);
        // let pagesCell = row.insertCell(5);

        idCell.innerHTML = allBooks[i].id;
        nameCell.innerHTML = allBooks[i].name;
        authorCell.innerHTML = allBooks[i].author;
        publisherCell.innerHTML = allBooks[i].publisher;
        colorCell.innerHTML = allBooks[i].color;
        // pagesCell.innerHTML = allBooks[i].numberOfPages;
    }
}