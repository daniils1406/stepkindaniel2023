package ru.itis.repositories;

import ru.itis.models.User;

import java.util.Optional;

public interface UserRep {
    void save(User user);

    Optional<User> findByEmail(String email);
}
