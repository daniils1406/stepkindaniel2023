package ru.itis.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import ru.itis.models.Book;

import javax.sql.DataSource;
import java.util.*;


@Repository
public class ProductsRepository implements BookRep {

    private static final String SQL_LIKE_BY_NAME_OR_AUTHOR = "SELECT * FROM book WHERE " +
            "(name_of_book ilike :query or author ilike :query)";

    private static final String SQL_FIND_MAX_SIZE_OF_ALL_BOOKS = "SELECT * FROM book WHERE number_of_pages=(SELECT MAX(number_of_pages) FROM book);";

    private static final String SQL_SELECT_ALL_BOOKS_BY_PAGES = "select * from book WHERE number_of_pages<:max AND number_of_pages>:min";

    private static final String SQL_SELECT_ALL_BOOKS = "select * from book";

    private static final String SQL_UPDATE_BOOK_BY_ID = "update book set name_of_book=:name, author=:author, publisher=:publisher, color=:color, number_of_pages = :pages where id = :id;";

    private static final String SQL_DELETE_BOOK_BY_ID = "delete from book where id=:id;";

    private static final String SQL_SELECT_SOME_BOOKS = "select * from book where author=:author and publisher=:publisher order by id desc;";


    private static final String SQL_SELECT_BY_ID = "select * from book where id = :id";


    private static final RowMapper<Book> bookMapper = (row, rowNumber) -> Book.builder()
            .id(row.getLong("id"))
            .name(row.getString("name_of_book"))
            .author(row.getString("author"))
            .publisher(row.getString("publisher"))
            .color(row.getString("color"))
            .numberOfPages(row.getInt("number_of_pages"))
            .build();

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public ProductsRepository(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public List<Book> findAll(String column, String desk) {
        if (column != null && desk != null) {
            String SQL_SELECT_ALL_BOOKS1 = SQL_SELECT_ALL_BOOKS + " order by " + column + " " + desk + "";
            return namedParameterJdbcTemplate.query(SQL_SELECT_ALL_BOOKS1, bookMapper);
        }
        return namedParameterJdbcTemplate.query(SQL_SELECT_ALL_BOOKS, bookMapper);
    }

    @Override
    public void save(Book book) {
        Map<String, Object> paramsAsMap = new HashMap<>();

        paramsAsMap.put("name_of_book", book.getName());
        paramsAsMap.put("author", book.getAuthor());
        paramsAsMap.put("publisher", book.getPublisher());
        paramsAsMap.put("color", book.getColor());
        paramsAsMap.put("number_of_pages", book.getNumberOfPages());

        SimpleJdbcInsert insert = new SimpleJdbcInsert(namedParameterJdbcTemplate.getJdbcTemplate());

        Long id = insert.withTableName("book")
                .usingGeneratedKeyColumns("id")
                .executeAndReturnKey(new MapSqlParameterSource(paramsAsMap)).longValue();
        book.setId(id);
    }

    @Override
    public Integer findMaxNumberOfPages() {
        if ((namedParameterJdbcTemplate.query(SQL_FIND_MAX_SIZE_OF_ALL_BOOKS, bookMapper).size() != 0)) {
            Book book = (namedParameterJdbcTemplate.query(SQL_FIND_MAX_SIZE_OF_ALL_BOOKS, bookMapper).get(0));
            return book.getNumberOfPages();
        } else {
            return 0;
        }
        
    }

    @Override
    public List<Book> findAllByPages(String from, String to, String column, String desc) {
        Map<String, Integer> p = new HashMap<>();
        String SQL_SELECT_ALL_BOOKS_BY_PAGES1 = null;
        if (column != null && desc != null) {
            SQL_SELECT_ALL_BOOKS_BY_PAGES1 = SQL_SELECT_ALL_BOOKS_BY_PAGES + " order by " + column + " " + desc + "";
        }
        if (from == null || from.equals("")) {
            p.put("min", 0);
        } else {
            p.put("min", Integer.valueOf(from));
        }
        if (to == null || to.equals("")) {
            p.put("max", findMaxNumberOfPages() + 1);
        } else {
            p.put("max", Integer.parseInt(to));
        }
        if (column != null && desc != null) {
            return namedParameterJdbcTemplate.query(SQL_SELECT_ALL_BOOKS_BY_PAGES1, p, bookMapper);
        } else {
            return namedParameterJdbcTemplate.query(SQL_SELECT_ALL_BOOKS_BY_PAGES, p, bookMapper);
        }

    }


    @Override
    public List<Book> findAllByNameAndPublisher(String author, String publisher) {
        Map<String, String> p = new HashMap<>();
        p.put("author", author);
        p.put("publisher", publisher);
        return namedParameterJdbcTemplate.query(SQL_SELECT_SOME_BOOKS, p, bookMapper);
    }

    @Override
    public List<Book> findAllByNameOrAuthorLike(String argument) {
        return namedParameterJdbcTemplate.query(SQL_LIKE_BY_NAME_OR_AUTHOR,
                Collections.singletonMap("query", "%" + argument + "%"),
                bookMapper);
    }

    @Override
    public Optional<Book> findById(Long id) {
        try {
            Map<String, Long> p = new HashMap<>();
            p.put("id", id);
            return Optional.ofNullable((Book) namedParameterJdbcTemplate.query(SQL_SELECT_BY_ID, p, bookMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }

    }


    @Override
    public void update(Book book) {
        Map<String, Object> p = new HashMap<>();
        p.put("name", book.getName());
        p.put("author", book.getAuthor());
        p.put("publisher", book.getPublisher());
        p.put("color", book.getColor());
        p.put("pages", book.getNumberOfPages());
        p.put("id", book.getId());
        int res1 = namedParameterJdbcTemplate.update(SQL_UPDATE_BOOK_BY_ID, p);
        if (res1 != 1) throw new IllegalArgumentException("Could not update");
    }

    @Override
    public void delete(Long id) {
        Map<String, Long> p = new HashMap<>();
        p.put("id", id);
        int res1 = namedParameterJdbcTemplate.update(SQL_DELETE_BOOK_BY_ID, p);
        if (res1 != 1) throw new IllegalArgumentException("Could not delete");
    }

}
