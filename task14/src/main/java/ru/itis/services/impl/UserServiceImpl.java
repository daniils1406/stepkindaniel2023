package ru.itis.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.dto.UserSignUpDto;
import ru.itis.models.User;
import ru.itis.repositories.UserRep;
import ru.itis.services.UserService;

@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {

    private final UserRep userRep;

    @Override
    public void save(UserSignUpDto userDto) {
        User user = User.builder()
                .email(userDto.getEmail())
                .password(userDto.getPassword())
                .build();
        userRep.save(user);
    }

}
