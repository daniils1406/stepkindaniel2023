package ru.itis.controllers;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.itis.dto.BookDto;
import ru.itis.dto.FullBookDto;
import ru.itis.dto.SignUpDto;
import ru.itis.services.BookService;
import ru.itis.services.SearchService;

import java.util.List;

@Controller
@RequestMapping("/books")
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class Books {

    BookService bookService;
    SearchService searchService;


    @RequestMapping(method = RequestMethod.GET)
    public String getBooksPage(Model model) {
        model.addAttribute("allBooks", bookService.getListOfBooks(null, null));
        return "books";
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public List<BookDto> addBook(@RequestBody SignUpDto bookDto) {
        System.out.println(bookDto.toString());
        bookService.signUp(bookDto);
        return bookService.getListOfBooks(null, null);
    }

    @RequestMapping("/search")
    @ResponseBody
    public List<BookDto> LiveSearch(@RequestParam("argument") String argument) {
        return searchService.searchBooks(argument);
    }
}