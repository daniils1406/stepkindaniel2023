package ru.itis.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RequestMapping("/search")
@Controller
public class Search {
    @RequestMapping(method = RequestMethod.GET)
    public String getSearchPage() {
        return "search";
    }
}
