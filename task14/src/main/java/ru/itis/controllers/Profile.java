package ru.itis.controllers;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/profile")
@RequiredArgsConstructor
public class Profile {

    @RequestMapping(method = RequestMethod.GET)
    public String ProfilePage(Model model) {
        return "profile";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String setOrder(Model model, HttpServletResponse resp, @RequestParam("orderColumn") String orderColumn, @RequestParam("order") String order) {
        Cookie orderCookie = new Cookie("orderColumn", orderColumn);
        Cookie orderCookie1 = new Cookie("order", order);
        orderCookie.setPath("/");
        orderCookie1.setPath("/");
        resp.addCookie(orderCookie1);
        resp.addCookie(orderCookie);
        return "redirect:/";
    }
}
