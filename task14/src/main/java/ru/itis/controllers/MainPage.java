package ru.itis.controllers;


import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.itis.services.BookService;
import ru.itis.services.UserService;

import java.util.Arrays;
import java.util.Optional;


@Controller
@RequestMapping("/")
//@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class MainPage {
    private final BookService bookService;

    public MainPage(BookService bookService) {
        this.bookService = bookService;
    }

    String from = null;
    String to = null;


    @RequestMapping(method = RequestMethod.GET)
    public String getMainPage(Model model, HttpServletRequest req) {
        Optional<String> column = Arrays.stream(req.getCookies()).filter((str) -> str.getName().equals("orderColumn")).map((str) -> str.getValue()).findAny();
        Optional<String> order = Arrays.stream(req.getCookies()).filter((str) -> str.getName().equals("order")).map((str) -> str.getValue()).findAny();

        if (!column.isEmpty() && !order.isEmpty()) {
            model.addAttribute("allBooks", bookService.getListOfBooksByArgument(from, to, column.get(), order.get()));
        } else {
            model.addAttribute("allBooks", bookService.getListOfBooksByArgument(from, to, null, null));
        }
        return "mainPage";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String setFromAndToForPagesCriteria(Model model, HttpServletRequest req, @RequestParam("from") String from, @RequestParam("to") String to) {
        this.from = from;
        this.to = to;
        return getMainPage(model, req);
    }


}
