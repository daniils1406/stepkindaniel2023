package ru.itis.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/criteria")
@RequiredArgsConstructor
public class Сriteria {

    @RequestMapping(method = RequestMethod.GET)
    public String CriteriaPage(Model model) {
        return "criteria";
    }
}
