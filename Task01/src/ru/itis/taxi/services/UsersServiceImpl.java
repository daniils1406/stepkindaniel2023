package ru.itis.taxi.services;

import ru.itis.taxi.dto.SignUpForm;
import ru.itis.taxi.models.User;
import ru.itis.taxi.repositories.UsersRepository;

import java.util.List;
import java.util.function.Function;

/**
 * 30.06.2022
 * 02. TaxiService
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;

    private final Function<SignUpForm, User> toUserMapper;

    public UsersServiceImpl(UsersRepository usersRepository, Function<SignUpForm, User> toUserMapper) {
        this.usersRepository = usersRepository;
        this.toUserMapper = toUserMapper;
    }

    @Override
    public void signUp(SignUpForm signUpForm) {
        User user = toUserMapper.apply(signUpForm);

        // TODO: сделать проверку, чтобы пользователя с таким Email-не было
        List<User> users=usersRepository.findAll();
        boolean EmailNotExist=true;
        int i=0;
        while(EmailNotExist && i<users.size()){
            if(users.get(i).getEmail().equals(user.getEmail())){
                EmailNotExist=false;
            }
            i++;
        }
        if(EmailNotExist){
            usersRepository.save(user);
        }else{
            System.out.println("Пользователь с такой почтой уже существует!");
        }
    }
}
