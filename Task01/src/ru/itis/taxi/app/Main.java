package ru.itis.taxi.app;

import ru.itis.taxi.dto.SignUpForm;
import ru.itis.taxi.mappers.Mappers;
import ru.itis.taxi.models.User;
import ru.itis.taxi.repositories.UsersRepository;
import ru.itis.taxi.repositories.UsersRepositoryFilesImpl;
import ru.itis.taxi.services.UsersService;
import ru.itis.taxi.services.UsersServiceImpl;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class Main {

    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFilesImpl("users.txt");
        UsersService usersService = new UsersServiceImpl(usersRepository, Mappers::fromSignUpForm);
        usersService.signUp(new SignUpForm("Антон", "Лукин",
                "lukovichka@gmail.com", "sdchjv21"));
        usersService.signUp(new SignUpForm("Марсель", "Сидиков",
                "sidikov.marsel@gmail.com", "qwerty007"));
        usersService.signUp(new SignUpForm("Рустам", "Шайхинуров",
                "risaatam@gmail.com", "vfsdh3"));
        usersService.signUp(new SignUpForm("Елена", "Липхен",
                "Elena0810@gmail.com", "jvfjb23"));
        List<User> p = usersRepository.findAll();
        User user1 = new User(p.get(1).getId(), "Даниил", "Степкин", "daniils1406@mail.ru", "asdc");
        usersRepository.update(user1);
        usersRepository.delete(user1);
        usersRepository.deleteById(p.get(2).getId());
        User user2 = usersRepository.findById(p.get(3).getId());
        System.out.println(user2.toString());


    }
}
