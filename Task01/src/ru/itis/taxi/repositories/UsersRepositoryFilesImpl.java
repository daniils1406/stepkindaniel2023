package ru.itis.taxi.repositories;

import ru.itis.taxi.models.User;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;

/**
 * 30.06.2022
 * 02. TaxiService
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class UsersRepositoryFilesImpl implements UsersRepository {

    private final String fileName;

    private static final Function<User, String> userToString = user ->
            user.getId()
                    + "|" + user.getFirstName()
                    + "|" + user.getLastName()
                    + "|" + user.getEmail()
                    + "|" + user.getPassword();

    public UsersRepositoryFilesImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<User> findAll() {
        LinkedList<User> users = new LinkedList<>();
        try (Reader reader = new FileReader(fileName);
             BufferedReader bufferedreader = new BufferedReader(reader)) {
            for (String stringUser = bufferedreader.readLine(); stringUser != null; stringUser = bufferedreader.readLine()) {
                String[] userDescription = stringUser.split("\\|");
                User user = new User(UUID.fromString(userDescription[0]), userDescription[1], userDescription[2], userDescription[3], userDescription[4]);
                users.add(user);
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return users;
    }

    @Override
    public void save(User entity) {
        entity.setId(UUID.randomUUID());
        try (Writer writer = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {

            String userAsString = userToString.apply(entity);
            bufferedWriter.write(userAsString);
            bufferedWriter.newLine();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(User entity) {
        List<User> users = findAll();
        boolean userWhichUpdate = false;
        int i = 0;
        while (!userWhichUpdate && i < users.size()) {
            if (users.get(i).getId().equals(entity.getId())) {
                userWhichUpdate = true;
            }
            i++;
        }
        i--;
        if (userWhichUpdate) {
            users.set(i, entity);
        }
        try {
            new FileWriter(fileName, false).close();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        try (Writer writer = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {
            for (i = 0; i < users.size(); i++) {
                bufferedWriter.write(userToString.apply(users.get(i)));
                bufferedWriter.newLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(User entity) {
        List<User> users = findAll();
        boolean userWhichDelete = false;
        int i = 0;
        while (!userWhichDelete && i < users.size()) {
            if (users.get(i).getFirstName().equals(entity.getFirstName()) &&
                    users.get(i).getLastName().equals(entity.getLastName()) &&
                    users.get(i).getEmail().equals(entity.getEmail()) &&
                    users.get(i).getPassword().equals(entity.getPassword())) {
                userWhichDelete = true;
            }
            i++;
        }
        i--;
        if (userWhichDelete) {
            users.remove(i);
        }
        try {
            new FileWriter(fileName, false).close();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        try (Writer writer = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {
            for (i = 0; i < users.size(); i++) {
                bufferedWriter.write(userToString.apply(users.get(i)));
                bufferedWriter.newLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void deleteById(UUID cod) {
        List<User> users = findAll();
        boolean userWhichDelete = false;
        int i = 0;
        while (!userWhichDelete && i < users.size()) {
            if (users.get(i).getId().equals(cod)) {
                userWhichDelete = true;
            }
            i++;
        }
        i--;
        if (userWhichDelete) {
            users.remove(i);
        }
        try {
            new FileWriter(fileName, false).close();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        try (Writer writer = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {
            for (i = 0; i < users.size(); i++) {
                bufferedWriter.write(userToString.apply(users.get(i)));
                bufferedWriter.newLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public User findById(UUID cod) {
        List<User> users = findAll();
        boolean userWhichFind = false;
        int i = 0;
        while (!userWhichFind && i < users.size()) {
            if (users.get(i).getId().equals(cod)) {
                userWhichFind = true;
            }
            i++;
        }
        i--;
        if (userWhichFind) {
            return users.get(i);
        }
        return null;
    }
}
