package ru.itis.repositories;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import ru.itis.models.Book;

import javax.sql.DataSource;
import java.util.*;

public class ProductsRepository implements BookRep{

    private static final String SQL_SELECT_ALL_BOOKS = "select * from book;";

    private static final String SQL_UPDATE_BOOK_BY_ID="update book set name_of_book=:name, author=:author, publisher=:publisher, color=:color, year_of_release = :year where id = :id;";

    private static final String SQL_DELETE_BOOK_BY_ID = "delete from book where id=:id;";

    private static final String SQL_SELECT_SOME_BOOKS = "select * from book where author=:author and publisher=:publisher order by id desc;";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from book where id = :id";


    //language=SQL
    private static final String SQL_SELECT_BY_AGE_IN_RANGE_ORDER_BY_ID_DESC =
            "select * from student where age > :minAge and age < :maxAge order by id desc";

    private static final RowMapper<Book> bookMapper = (row, rowNumber) -> Book.builder()
            .id(row.getLong("id"))
            .name(row.getString("name_of_book"))
            .author(row.getString("author"))
            .publisher(row.getString("publisher"))
            .color(row.getString("color"))
            .build();

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public ProductsRepository(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public List<Book> findAll() {
        return namedParameterJdbcTemplate.query(SQL_SELECT_ALL_BOOKS, bookMapper);
    }


    @Override
    public void save(Book book) {
        Map<String, Object> paramsAsMap = new HashMap<>();

        paramsAsMap.put("name_of_book", book.getName());
        paramsAsMap.put("author", book.getAuthor());
        paramsAsMap.put("publisher", book.getPublisher());
        paramsAsMap.put("color", book.getColor());
        paramsAsMap.put("year_of_release", book.getYearOfRelease());

        SimpleJdbcInsert insert = new SimpleJdbcInsert(namedParameterJdbcTemplate.getJdbcTemplate());

        Long id = insert.withTableName("book")
                .usingGeneratedKeyColumns("id")
                .executeAndReturnKey(new MapSqlParameterSource(paramsAsMap)).longValue();


        book.setId(id);
    }

    @Override
    public Optional<Book> findById(Long id) {
        try{
            Map<String,Long> p=new HashMap<>();
            p.put("id",id);
            return Optional.ofNullable((Book) namedParameterJdbcTemplate.query(SQL_SELECT_BY_ID,p,bookMapper));
        }catch (EmptyResultDataAccessException e){
            return Optional.empty();
        }

    }

    @Override
    public void update(Book book) {
        Map<String,Object> p=new HashMap<>();
        p.put("name",book.getName());
        p.put("author", book.getAuthor());
        p.put("publisher", book.getPublisher());
        p.put("color", book.getColor());
        p.put("year", book.getYearOfRelease());
        p.put("id", book.getId());
        int res1=namedParameterJdbcTemplate.update(SQL_UPDATE_BOOK_BY_ID,p);
        if(res1!=1) throw new IllegalArgumentException("Could not update");
    }

    @Override
    public void delete(Long id) {
        Map<String,Long> p=new HashMap<>();
        p.put("id", id);
        int res1= namedParameterJdbcTemplate.update(SQL_DELETE_BOOK_BY_ID,p);
        if (res1 != 1) throw new IllegalArgumentException("Could not delete");
    }

    @Override
    public List<Book> findAllByNameAndPublisher(String author, String publisher) {
        Map<String,String> p=new HashMap<>();
        p.put("author",author);
        p.put("publisher", publisher);
        return namedParameterJdbcTemplate.query(SQL_SELECT_SOME_BOOKS,p,bookMapper);
    }
}
