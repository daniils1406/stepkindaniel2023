package ru.itis.repositories;

import ru.itis.models.Book;
import ru.itis.models.Student;

import java.util.List;
import java.util.Optional;

public interface BookRep {
    List<Book> findAll();

    void save(Book book);

    Optional<Book> findById(Long id);

    void update(Book book);

    void delete(Long id);

    public List<Book> findAllByNameAndPublisher(String name, String publisher);

}
