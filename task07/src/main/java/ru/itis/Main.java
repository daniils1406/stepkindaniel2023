package ru.itis;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.itis.models.Book;
import ru.itis.repositories.ProductsRepository;
import ru.itis.repositories.StudentsRepository;
import ru.itis.repositories.StudentsRepositoryJdbcTemplateImpl;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

public class Main {


    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(Main.class.getResourceAsStream("/db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(properties.getProperty("db.url"));
        config.setUsername(properties.getProperty("db.username"));
        config.setPassword(properties.getProperty("db.password"));
        config.setMaximumPoolSize(20);

        HikariDataSource dataSource = new HikariDataSource(config);


        ProductsRepository productsRepository =new ProductsRepository(dataSource);

        Book book1= Book.builder()
                .name("Война и мир")
                .publisher("АСТ")
                .author("Толстой Л.В.")
                .color("коричневый")
                .build();


        Book book2= Book.builder()
                .name("Анна Коренина")
                .publisher("АСТ")
                .author("Толстой Л.В.")
                .color("коричневый")
                .build();

        Book book3= Book.builder()
                .name("Мертвые души")
                .publisher("ЛИТРЕС")
                .author("Гоголь Н.В.")
                .color("черный")
                .build();

        Book book4= Book.builder()
                .name("Матренин двор")
                .publisher("АЛТАПРЕСС")
                .author("Солженицын А.И.")
                .color("желтый")
                .build();

        Book book5= Book.builder()
                .id(2L)
                .name("Война и мир")
                .publisher("АЛТАПРЕСС")
                .author("Толстой Л. В.1")
                .color("желтый")
                .yearOfRelease(1988)
                .build();

//        productsRepository.save(book1);
//        productsRepository.save(book2);
//        productsRepository.save(book3);
//        productsRepository.save(book4);



//        productsRepository.update(book5);

//        productsRepository.delete(1L);

//        List<Book> p=productsRepository.findAll();
//        for(Book k: p){
//            System.out.println(k.toString());
//        }

        List<Book> o=productsRepository.findAllByNameAndPublisher("Толстой Л.В.","АСТ");
        for(Book e: o){
            System.out.println(e.toString());
        }






//        StudentsRepository studentsRepository = new StudentsRepositoryJdbcTemplateImpl(dataSource);
//        System.out.println(studentsRepository.findAllByAgeInRangeOrderByIdDesc(21, 28));

//        ExecutorService executorService = Executors.newFixedThreadPool(50);
//
//        for (int i = 0; i < 10_000; i++) {
//            executorService.submit(() -> {
//              try {
//                  studentsRepository.save(Student.builder()
//                          .firstName("Marsel")
//                          .lastName("Sidikov")
//                          .build());
//              } catch (Exception e) {
//                  e.printStackTrace();
//                  System.exit(0);
//              }
//            });
//        }

    }
}
