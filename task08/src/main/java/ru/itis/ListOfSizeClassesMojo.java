package ru.itis;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Mojo(name = "list-of-classes", defaultPhase = LifecyclePhase.COMPILE)
public class ListOfSizeClassesMojo extends AbstractMojo {

    @Parameter(defaultValue = "${project.build.outputDirectory}", required = true)
    private String outputFolderFileName;

    @Parameter(defaultValue = "${project.build.sourceDirectory}", required = true, readonly = true)
    private String sourceFolderFileName;

    @Parameter(name = "listOfClassesFileName", required = true)
    private String listOfClassesFileName;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        File outputFolder = new File(outputFolderFileName);

        File listOfClassesFile = new File(outputFolder, listOfClassesFileName);

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(listOfClassesFile))) {
            getLog().info("Output file for list of classes is - " + listOfClassesFileName);

            Files.walk(Paths.get(sourceFolderFileName))
                    .filter(Files::isRegularFile)
                    .forEach(file -> {
                        try {
                            writer.write(file.getFileName().toString());
                            writer.write("  Size: ");
                            writer.write(String.valueOf(file.toFile().length()));
                            writer.newLine();
                        } catch (IOException e) {
                            throw new IllegalArgumentException(e);
                        }
                    });
            getLog().info("Finish work");
        } catch (IOException e) {
            throw new MojoExecutionException(e);
        }
    }
}
