задание 1
select c.model as model, c.speed as speed, c.hd as hd
from pc c
where c.price<500 and c.model in(
    select s.model
    from pc s
    where s.model in(
    select p.model
    from product p
    where p.type='pc')
    )


задание 2
select distinct c.maker
from product c
where c.type='printer'

Задание 3
select c.model as model, c.ram as ram, c.screen as screen
from laptop c
where c.price>1000 and c.model in(
    select s.model
    from laptop s
    where s.model in(
    select p.model
    from product p
    where p.type='laptop')
    )

Задание 4
select *
from printer c
where c.color='y'

Задание 5
select c.model as model, c.speed as speed, c.hd as hd
from pc c
where c.price<600 and (c.cd='12x' or c.cd='24x')

Задание 6
select distinct product.maker, laptop.speed
from product inner join laptop on product.model=laptop.model
where laptop.hd>=10

Задание 7
SELECT distinct p.model, pc.price FROM pc JOIN Product p ON PC.model=p.model WHERE maker='B'
UNION ALL
SELECT distinct p.model, pr.price FROM printer pr JOIN Product p ON pr.model=p.model WHERE maker='B'
UNION ALL
SELECT distinct p.model, lp.price FROM laptop lp JOIN Product p ON lp.model=p.model WHERE maker='B'

Задание 8
SELECT distinct product.maker
from product
where product.type='pc' and product.maker not in(
    select distinct product.maker
    from product
    where product.type in(
        select product.type
        from product
        where product.type='laptop'
        )
    )

Задание 9
select distinct product.maker
from product
where product.model in(
    select pc.model
    from pc
    where pc.speed>=450
    )
Задание 10
select printer.model, printer.price
from printer
where printer.price in(
    select max(printer.price)
    from printer
    )


Задание 11
select avg(pc.speed) as speed
from pc

Задние 12
select avg(laptop.speed)
from laptop
where laptop.price>1000

Задание 13
select avg(pc.speed)
from pc
where pc.model in (
    select product.model
    from product
    where product.maker='A'
    )

Задание 14 
select ships.class, ships.name,classes.country
from ships inner join classes on classes.class=ships.class 
where ships.class in(
    select classes.class
    from classes
    where classes.numGuns>=10
    )
Задание 15
SELECT hd
FROM pc
GROUP BY hd
HAVING count(hd)>1

Задание 16
SELECT DISTINCT c.model AS model_1, d.model AS model_2,c.speed,c.ram
FROM PC AS c, PC d
where c.speed=d.speed and c.ram=d.ram and c.model>d.model

Задание 17
select distinct product.type, laptop.model, laptop.speed
from laptop,product
where product.type='laptop' and laptop.speed<(
    select min(pc.speed)
    from pc
    )


Задание 18
select distinct product.maker, printer.price
from printer, product
where printer.model=product.model and product.type='printer'and printer.color='y' and printer.price in(
    select min(printer.price)
    from printer
    where printer.color='y'
)

Задание 19
select distinct product.maker, avg(laptop.screen)
from product, laptop
where laptop.model=product.model
group by product.maker

Задание 20
SELECT maker, COUNT(model)
FROM product
WHERE type = 'pc'
GROUP BY product.maker
HAVING COUNT (DISTINCT model) >= 3

Задание 21
select distinct product.maker, max(pc.price)
from pc, product
where pc.model=product.model and product.type='pc'
group by product.maker

Задание 22
select pc.speed, avg(pc.price)
from pc
where pc.speed>600
group by pc.speed

Задание 23
select distinct product.maker
from product,pc,laptop
where pc.model=product.model and pc.speed>=750 and product.maker in(
    select product.maker
    from laptop,product
    where product.model=laptop.model and laptop.speed>=750
    )

Задание 24
SELECT model
FROM (
 SELECT model, price
 FROM pc
 UNION
 SELECT model, price
 FROM Laptop
 UNION
 SELECT model, price
 FROM Printer
) t1
WHERE price = (
 SELECT MAX(price)
 FROM (
  SELECT price
  FROM pc
  UNION
  SELECT price
  FROM Laptop
  UNION
  SELECT price
  FROM Printer
  ) t2
 )

Задание 25
select distinct product.maker
from product,printer
where product.type='printer' and product.maker in(
    select product.maker
    from product,pc
    where product.type='pc' and pc.model=product.model and pc.model in(
        select pc.model
        from pc
        where pc.ram in(
            select min(pc.ram)
            from pc
            ) and pc.speed in (
                select max(pc.speed)
                from pc
                where pc.ram in(
                    select min(pc.ram)
                    from pc
                    )
            )
        )
    )
group by product.maker

Задание 26

select avg(c.price)
from (select pc.price
      from pc,product
      where pc.model=product.model and product.maker='A'
      union all
      select laptop.price
      from laptop,product
      where laptop.model=product.model and product.maker='A'
      ) as c

Задание 27
select distinct product.maker, avg(pc.hd)
from product,pc
where product.model=pc.model and product.maker in(
    select product.maker
    from product
    where product.type='printer'
    )
group by maker

Задание28

select count(maker)
from product
where maker in
      (
          Select maker 
          from product
          group by maker
          having count(model) = 1
      )

Задание 29

SELECT t1.point, t1.date, inc, out
FROM income_o t1 LEFT JOIN outcome_o t2 ON t1.point = t2.point
AND t1.date = t2.date
UNION
SELECT t2.point, t2.date, inc, out
FROM income_o t1 RIGHT JOIN outcome_o t2 ON t1.point = t2.point
AND t1.date = t2.date

задание 30
select point, date, SUM(sum_out), SUM(sum_inc)
from( select point, date, SUM(inc) as sum_inc, null as sum_out from Income Group by point, date
Union
select point, date, null as sum_inc, SUM(out) as sum_out from Outcome Group by point, date ) as t
group by point, date order by point

Задание 31
select classes.class, classes.country
from classes
where classes.bore>=16


Задание 33
select outcomes.ship
from outcomes
where outcomes.battle='North Atlantic' and outcomes.result='sunk'

Задание 34
select distinct ships.name
from ships,classes
where ships.launched is not null and ships.launched>=1922 and ships.class in(
    select classes.class
    from classes
    where classes.displacement>35000 and classes.type='bb'
    )

Задание 35
SELECT model, type
FROM product
WHERE upper(model) like '%[^A-Z]%' and model not like '%[^0-9]%'
OR model not like '%[^A-Z]%' and model like '%[^0-9]%'

Задание 36
select ships.name
from ships,classes
where classes.class=ships.name
union
select outcomes.ship
from outcomes,classes
where classes.class = outcomes.ship

Задание 37
select c.class
FROM classes c
 LEFT JOIN (
 SELECT class, name
 FROM ships
 UNION
 SELECT ship, ship
 FROM outcomes
) AS s ON s.class = c.class
GROUP BY c.class
HAVING COUNT(s.name) = 1

Задание 38
select distinct classes.country
from classes 
where classes.type='bb'
INTERSECT
select distinct classes.country
from classes 
where classes.type='bc'


Задание 42
SELECT outcomes.ship, outcomes.battle 
FROM outcomes 
WHERE outcomes.result = 'sunk'

Задание 43
select name
from battles
where year(battles.date) not in
     (select ships.launched
      from ships
      where ships.launched is not null)
Задание 44
select ships.name 
from ships
where ships.name like 'R%'
UNION
select outcomes.ship 
from outcomes
where outcomes.ship like 'R%'



