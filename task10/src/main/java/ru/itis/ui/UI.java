package ru.itis.ui;

import ru.itis.models.Book;
import ru.itis.services.UserService;

import java.util.Scanner;

public class UI {

    private final Scanner scanner = new Scanner(System.in);

    private final UserService usersService;

    public UI(UserService usersService) {
        this.usersService = usersService;
    }

    public void start() {
        while (true) {
            printMainMenu();

            String command = scanner.nextLine();

            switch (command) {
                case "1" -> {
                    String name = scanner.nextLine();
                    String author = scanner.nextLine();
                    String publisher = scanner.nextLine();
                    String color = scanner.nextLine();
                    Integer yearOfRelease = Integer.valueOf(scanner.nextLine());
                    this.usersService.signUp(name,author,publisher,color,yearOfRelease);
                    System.out.println("Книга зарегистрирована");
                }
                case "2" -> {
                    if (!usersService.getListOfBooks().isEmpty()){
                        for(Book book:usersService.getListOfBooks()){
                            System.out.println(book.toString());
                        }
                    }else {
                        System.out.println("Книг на складе нет!");
                    }

                }
                case "3" -> {
                    System.out.println("Укажите id товара который хотите удалить");
                    if(usersService.deleteById(Long.parseLong(scanner.nextLine()))){
                        System.out.println("Товар удален");
                    }else{
                        System.out.println("Такого товара нет");
                    }
                }
                case "4" -> System.exit(0);
                default -> System.out.println("Команда не распознана");
            }
        }


    }

    private void printMainMenu() {
        System.out.println("Выберите действие:");
        System.out.println("1. Добавить товар");
        System.out.println("2. Посмотреть список товаров");
        System.out.println("3. Удалить товар по id");
        System.out.println("4. Выйти");
    }
}
