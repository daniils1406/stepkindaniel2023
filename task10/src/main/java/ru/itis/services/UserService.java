package ru.itis.services;

import ru.itis.models.Book;

import java.util.List;

public interface UserService {
    void signUp(String name, String author, String publisher,String color, Integer yearOfRelease);
    List<Book> getListOfBooks();

    boolean deleteById(Long id);
}
