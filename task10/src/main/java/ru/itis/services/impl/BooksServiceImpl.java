package ru.itis.services.impl;

import ru.itis.models.Book;
import ru.itis.repositories.BookRep;
import ru.itis.services.UserService;

import java.util.List;

public class BooksServiceImpl implements UserService {

    BookRep bookRep;

    public BooksServiceImpl(BookRep bookRep){
        this.bookRep=bookRep;
    }
    @Override
    public void signUp(String name, String author, String publisher, String color, Integer yearOfRelease) {
        Book book=Book.builder()
                .name(name)
                .author(author)
                .publisher(publisher)
                .color(color)
                .yearOfRelease(yearOfRelease)
                .build();
        bookRep.save(book);
    }

    public List<Book> getListOfBooks(){
        return bookRep.findAll();
    }

    @Override
    public boolean deleteById(Long id) {
        try {
            bookRep.delete(id);
            return true;
        }catch (IllegalArgumentException e){
            return false;
        }
    }
}
