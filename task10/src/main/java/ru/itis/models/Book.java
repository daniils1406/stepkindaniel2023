package ru.itis.models;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Book {
    private Long id;
    private String name;
    private String author;
    private String publisher;
    private String color;
    private Integer yearOfRelease;

}
