drop table if exists book;


-- создание таблицы
create table book
(
    id         bigserial primary key, -- идентификатор строки - всегда уникальный
    name_of_book        varchar(30),
    author              varchar(30),
    publisher           varchar(30),
    color               varchar(20),
    year_of_release     INTEGER
);
