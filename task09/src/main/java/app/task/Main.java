package app.task;

import app.link.PostgresProvider;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class Main {
    public static void main(String[] args){
        try {
            Connection connection= PostgresProvider.getConnection();
            Statement statement=connection.createStatement();
            statement.execute(SqlGenerator.createTable(User.class));
            statement.execute(SqlGenerator.createTable(Book.class));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        try {
            User user=new User();
            Class<?> user1=Class.forName("app.task.User");
            Field[] fields= user1.getDeclaredFields();
            for(int i=0;i< fields.length;i++){
                fields[i].setAccessible(true);
            }
            fields[1].set(user,"Marsel");
            fields[2].set(user,"Sidikov");
            fields[3].set(user,true);
            Book book=new Book();
            Class<?> book1=book.getClass();
            fields=book1.getDeclaredFields();
            for(Field field: fields){
                field.setAccessible(true);
            }
            fields[1].set(book,1968);
            fields[2].set(book,"Толстой");
            fields[3].set(book, "СССР");
            fields[4].set(book,true);
            Connection connection= PostgresProvider.getConnection();
            Statement statement=connection.createStatement();
            statement.execute(SqlGenerator.insert(user));
            statement.execute(SqlGenerator.insert(book));
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}