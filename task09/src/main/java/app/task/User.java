package app.task;

@TableName(nameOfTable = "account")
public class User {
    @ColumnName(name="id", primary = true, identity = true)
    private Long id;
    @ColumnName(name = "first_name", maxLength = 25)
    private String firstName;
    @ColumnName(name = "last_name")
    private String lastName;
    @ColumnName(name="is_worker", defaultBoolean = true)
    private boolean isWorker;
}