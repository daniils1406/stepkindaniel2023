package app.task;
@TableName(nameOfTable = "Book")
public class Book {
    @ColumnName(name = "id",primary = true,identity = true)
    private Long id;
    @ColumnName(name = "year_of_publication")
    private Integer yearOfPublication;
    @ColumnName(name = "author")
    private String author;
    @ColumnName(name = "publisher")
    private String publisher;
    @ColumnName(name = "inStock",defaultBoolean = true)
    private boolean inStock;
}