package app.task;

import java.lang.reflect.Field;

public class SqlGenerator {
    static <T> String createTable(Class<T> entityClass){
        String str;
        str="CREATE TABLE IF NOT EXISTS "+entityClass.getDeclaredAnnotation(TableName.class).nameOfTable()+"" +
                "(";
        Field[] fields=entityClass.getDeclaredFields();
        for(int i=0;i<fields.length;i++){
            str=str+stringBuilder(fields[i]);
            if(i!= fields.length-1){
                str=str+",";
            }
        }
        str=str+");";
        return str;
    }
    static String insert(Object entity){
        Class<?> user=entity.getClass();
        String str;
        str="INSERT INTO "+user.getDeclaredAnnotation(TableName.class).nameOfTable()+" (";
        Field[] fields=user.getDeclaredFields();
        for(int i=0;i< fields.length;i++){
            if(!fields[i].getDeclaredAnnotation(ColumnName.class).name().equals("id")) {
                if (i != fields.length - 1) {
                    str = str + "" + fields[i].getDeclaredAnnotation(ColumnName.class).name() + ", ";
                } else {
                    str = str + "" + fields[i].getDeclaredAnnotation(ColumnName.class).name() + ") VALUES (";
                }
            }
        }
        try{
            for (int i=0;i< fields.length;i++){
                fields[i].setAccessible(true);
                if(!fields[i].getDeclaredAnnotation(ColumnName.class).name().equals("id")) {
                    if (i != fields.length - 1) {
                        if (fields[i].getType().equals(String.class)) {
                            str = str + "\'" + fields[i].get(entity) + "\', ";
                        } else {
                            str = str + "" + fields[i].get(entity) + ", ";
                        }
                    } else {
                        if (fields[i].getType().equals(String.class)) {
                            str = str + "\'" + fields[i].get(entity) + "\');";
                        } else {
                            str = str + "" + fields[i].get(entity) + ");";
                        }
                    }
                }
            }
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        return str;
    }



    private static String stringBuilder(Field field) {
        String column;
        ColumnName columnName=field.getDeclaredAnnotation(ColumnName.class);
        column=""+columnName.name()+"";

        if(columnName.identity()) {
            column = column + " bigserial";
            if (columnName.primary()) {
                column = column + " primary key";
            }
        } else if (field.getType().equals(Long.class)) {
            column=column+" bigint";
            if(columnName.primary()){
                column=column+" primary key";
            }

    } else if (field.getType().equals(Integer.class)) {
            column=column+" INT";
            if(columnName.primary() && columnName.identity()){
                column=column+" primary key";
            }
        } else if (field.getType().equals(String.class)) {
            column=column+" varchar";
            if(columnName.maxLength()!=0){
                column=column+"("+columnName.maxLength()+")";
            }
        } else if (field.getType().equals(Boolean.TYPE)) {
            if(field.getDeclaredAnnotation(ColumnName.class).defaultBoolean()){
                column=column+" BOOLEAN DEFAULT true";
            }else{
                column=column+" BOOLEAN";
            }
        }
        return column;

    }
}
