package app.link;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class PostgresProvider {
    public static Connection getConnection() {
        try {
            Class.forName(PstgresProp.DRIVER);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        Connection connection;
        try {
            connection= DriverManager.getConnection(PstgresProp.URL,PstgresProp.USERNAME,PstgresProp.PASSWORD);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return connection;
    }
}
