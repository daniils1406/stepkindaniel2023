drop table if exists booking;
drop table if exists driver;
drop table if exists car;
drop table if exists customer;

create table customer
(
    id           serial primary key,
    first_name   char(20),
    rating       float check ( rating > 0.0 and rating <= 5.0 ),
    phone_number char(20)
);

create table driver
(
    id           serial primary key,
    first_name   char(20),
    rating       float check ( rating > 0.0 and rating <= 5.0 ),
    phone_number char(20)
);

create table car
(
    id             serial primary key,
    color          char(20),
    brand          char(20),
    garage_Position integer unique
);

create table booking
(
    id          serial primary key,
    start_date  date,
    start_time  time,
    finish_time time,
    price       integer,
    driver_id   integer,
    CONSTRAINT driver_id foreign key (driver_id) references driver (id),
    customer_id integer,
    CONSTRAINT customer_id foreign key (customer_id) references customer (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);



alter table customer
    add email char(20) not null default '';

alter table driver
    add car_id integer;

alter table driver
    add foreign key (car_id) references car (id);