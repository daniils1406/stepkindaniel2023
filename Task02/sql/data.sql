insert into customer(first_name, rating, phone_number)
values ('Мансур', 5.0, 72412423522);
insert into customer(first_name, rating, phone_number)
values ('Михаил', 2.7, 72412467732);
insert into customer(first_name, rating, phone_number)
values ('Гузель', 4.5, 7235732355);
insert into customer(first_name, rating, phone_number)
values ('Генадий', 3.9, 71412414547);

update customer
set email='mishka1204@mail.ru'
where id = 2;

insert into driver(first_name, rating, phone_number)
values ('Валентин', 5.0, 72412583652);
insert into driver(first_name, rating, phone_number)
values ('Сергей', 1.7, 72362467722);
insert into driver(first_name, rating, phone_number)
values ('Виктория', 4.2, 72357321251);
insert into driver(first_name, rating, phone_number)
values ('Семен', 2.9, 71242414547);


insert into car(color, brand, garage_Position)
values ('Черная', 'lada', 36);
insert into car(color, brand, garage_Position)
values ('Пурпурная', 'hinday', 21);
insert into car(color, brand, garage_Position)
values ('Ораньжевая', 'reno', 41);
insert into car(color, brand, garage_Position)
values ('Серая', 'volkswagen', 25);

update driver
set car_id=2
where id = 2;

update driver
set car_id=4
where id = 1;

update driver
set car_id=3
where id = 3;

update driver
set car_id=1
where id = 4;

insert into booking(start_date, start_time, finish_time, price, driver_id, customer_id)
values ('2021-02-02', '12:30', '13:00', 241, 1, 4);
insert into booking(start_date, start_time, finish_time, price, driver_id, customer_id)
values ('2021-03-02', '11:30', '13:00', 342, 2, 3);
insert into booking(start_date, start_time, finish_time, price, driver_id, customer_id)
values ('2021-03-01', '19:30', '20:00', 124, 4, 2);
insert into booking(start_date, start_time, finish_time, price, driver_id, customer_id)
values ('2021-01-25', '12:30', '13:00', 242, 3, 1);