package ru.itis.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.dto.BookDto;
import ru.itis.dto.FullBookDto;
import ru.itis.dto.SignUpDto;
import ru.itis.models.Book;
import ru.itis.repositories.BookRep;
import ru.itis.services.BookService;

import java.util.List;

import static ru.itis.dto.FullBookDto.fromFullBook;

@RequiredArgsConstructor
@Service
public class BooksServiceImpl implements BookService {

    final BookRep bookRep;

    @Override
    public void signUp(SignUpDto signUpDto) {
        Book book = Book.builder()
                .name(signUpDto.getName())
                .author(signUpDto.getAuthor())
                .publisher(signUpDto.getPublisher())
                .color(signUpDto.getColor())
                .numberOfPages(signUpDto.getNumberOfPages())
                .build();
        bookRep.save(book);
    }

    public List<FullBookDto> getListOfBooks() {
        return fromFullBook(bookRep.findAll());
    }

    public List<FullBookDto> getListOfBooksByArgument(String from, String to) {
        return fromFullBook(bookRep.findAllByPages(from, to));
    }


    @Override
    public boolean deleteById(Long id) {
        try {
            bookRep.delete(id);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }
}
