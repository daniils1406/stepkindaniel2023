package ru.itis.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.dto.BookDto;
import ru.itis.repositories.ProductsRepository;
import ru.itis.services.SearchService;

import java.util.Collections;
import java.util.List;

import static ru.itis.dto.BookDto.from;


@RequiredArgsConstructor
@Service
public class SearchServiceImpl implements SearchService {

    private final ProductsRepository productRepository;

    @Override
    public List<BookDto> searchBooks(String argument) {
        if (argument == null || argument.equals("")) {
            return Collections.emptyList();
        }
        return from(productRepository.findAllByNameOrAuthorLike(argument));
    }
}
