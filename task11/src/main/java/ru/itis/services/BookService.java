package ru.itis.services;

import ru.itis.dto.BookDto;
import ru.itis.dto.FullBookDto;
import ru.itis.dto.SignUpDto;
import ru.itis.models.Book;

import java.util.List;

public interface BookService {
    void signUp(SignUpDto signUpDto);
    List<FullBookDto> getListOfBooks();

    List<FullBookDto> getListOfBooksByArgument(String from,String to);

    boolean deleteById(Long id);
}
