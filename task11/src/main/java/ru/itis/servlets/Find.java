//package ru.itis.servlets;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import jakarta.servlet.ServletConfig;
//import jakarta.servlet.ServletException;
//import jakarta.servlet.annotation.WebServlet;
//import jakarta.servlet.http.HttpServlet;
//import jakarta.servlet.http.HttpServletRequest;
//import jakarta.servlet.http.HttpServletResponse;
//import org.springframework.context.ApplicationContext;
//import ru.itis.dto.FullBookDto;
//import ru.itis.services.BookService;
//import ru.itis.services.impl.BooksServiceImpl;
//
//import java.io.IOException;
//import java.util.List;
//
//@WebServlet("/find")
//public class Find extends HttpServlet {
//    BookService bookService;
//    private ObjectMapper objectMapper;
//
//    @Override
//    public void init(ServletConfig config) throws ServletException {
//        ApplicationContext context=(ApplicationContext) config.getServletContext().getAttribute("springContext");
//        bookService=context.getBean(BooksServiceImpl.class);
//        objectMapper=context.getBean(ObjectMapper.class);
//    }
//
//    @Override
//    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        String from=request.getParameter("from");
//        String to=request.getParameter("to");
//        System.out.println(from+" "+to);
//        List<FullBookDto> allBooks=bookService.getListOfBooksByArgument(from,to);
//        System.out.println(allBooks.toString());
//        String jsonResponse = objectMapper.writeValueAsString(allBooks);
//        System.out.println(jsonResponse);
//        response.setContentType("application/json");
//        response.getWriter().write(jsonResponse);
////        request.getRequestDispatcher("main.html").forward(request,response);
//    }
//}
