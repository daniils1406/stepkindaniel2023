package ru.itis.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import ru.itis.dto.BookDto;
import ru.itis.services.SearchService;

import java.io.IOException;
import java.util.List;

import static ru.itis.constants.Paths.*;

@WebServlet(name = "searchServlet", urlPatterns = {USERS_SEARCH_PATH}, loadOnStartup = 1)
public class SearchServlet extends HttpServlet {

    private SearchService searchService;

    private ObjectMapper objectMapper;

    @Override
    public void init(ServletConfig config) {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.searchService = context.getBean(SearchService.class);
        this.objectMapper = context.getBean(ObjectMapper.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String query = request.getParameter("argument");
        List<BookDto> books = searchService.searchBooks(query);
        String jsonResponse = objectMapper.writeValueAsString(books);
        response.setContentType("application/json");
        response.getWriter().write(jsonResponse);
    }
}
