package ru.itis.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import ru.itis.dto.FullBookDto;
import ru.itis.models.Book;
import ru.itis.services.BookService;
import ru.itis.services.impl.BooksServiceImpl;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;


@WebServlet("")
public class MainPage extends HttpServlet {
    BookService bookService;
    private ObjectMapper objectMapper;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        bookService = context.getBean(BooksServiceImpl.class);
        objectMapper = context.getBean(ObjectMapper.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter writer = resp.getWriter();
        String from = req.getParameter("from");
        String to = req.getParameter("to");
        List<FullBookDto> allBooks = bookService.getListOfBooksByArgument(from, to);

        String html = getHtmlForUsers(allBooks);

        writer.println(html);
    }


    private static String getHtmlForUsers(List<FullBookDto> books) {
        StringBuilder html = new StringBuilder();

        html.append("<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "\t<meta charset=\"utf-8\">\n" +
                "\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                "\t<title>Users</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<button onclick=\"location.href='/app/signUp.html'\">Перейти к форме</button>\n" +
                "<button onclick=\"location.href='/app/Find.html'\">Перейти к критерию</button>\n" +
                "<button onclick=\"location.href='/app/books/search.html'\">Перейти к поиску</button>\n" +
                "<table>\n" +
                "\t<tr>\n" +
                "\t\t<th>ID</th>\n" +
                "\t\t<th>Name</th>\n" +
                "\t\t<th>Author</th>\n" +
                "\t\t<th>Publisher</th>\n" +
                "\t\t<th>Color</th>\n" +
                "\t\t<th>YearOfRelease</th>\n" +
                "\t</tr>");

        for (FullBookDto book : books) {
            html.append("<tr>\n");
            html.append("<td>").append(book.getId()).append("</td>\n");
            html.append("<td>").append(book.getName()).append("</td>\n");
            html.append("<td>").append(book.getAuthor()).append("</td>\n");
            html.append("<td>").append(book.getPublisher()).append("</td>\n");
            html.append("<td>").append(book.getColor()).append("</td>\n");
            html.append("<td>").append(book.getNumberOfPages()).append("</td>\n");
            html.append("</tr>\n");
        }

        html.append("</table>\n" +
                "</body>\n" +
                "</html>");
        return html.toString();
    }
}
