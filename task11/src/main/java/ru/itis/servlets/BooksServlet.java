package ru.itis.servlets;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import ru.itis.dto.SignUpDto;
import ru.itis.dto.converters.http.HttpFormsConverter;
import ru.itis.services.BookService;

import java.io.IOException;

import static ru.itis.constants.Paths.*;

@WebServlet(name = "usersServlet", urlPatterns = {USERS_PATH, SIGN_UP_PATH}, loadOnStartup = 1)
public class BooksServlet extends HttpServlet {
    private BookService booksService;

    @Override
    public void init(ServletConfig config) {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.booksService = context.getBean(BookService.class);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getRequestURI().equals(APPLICATION_PREFIX + SIGN_UP_PATH)) {
            SignUpDto signUpData = HttpFormsConverter.from(request);

            booksService.signUp(signUpData);

            response.sendRedirect(APPLICATION_PREFIX);
        } else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }
}
