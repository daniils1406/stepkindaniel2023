drop table if exists book;


create table book
(
    id         bigserial primary key,
    name_of_book        varchar(30),
    author              varchar(30),
    publisher           varchar(30),
    color               varchar(20),
    number_of_pages     INTEGER
);
