package ru.itis.dto.converters.http;

import jakarta.servlet.http.HttpServletRequest;
import ru.itis.dto.SignUpDto;
import ru.itis.dto.UserSignUpDto;

public class HttpFormsConverter {
    public static SignUpDto from(HttpServletRequest request) {
        return SignUpDto.builder()
                .name(request.getParameter("nameOfBook"))
                .author(request.getParameter("author"))
                .publisher(request.getParameter("publisher"))
                .color(request.getParameter("color"))
                .numberOfPages(Integer.parseInt(request.getParameter("pages")))
                .build();
    }

    public static UserSignUpDto formUser(HttpServletRequest request) {
        return UserSignUpDto.builder()
                .email(request.getParameter("email"))
                .password(request.getParameter("password"))
                .build();
    }
}
