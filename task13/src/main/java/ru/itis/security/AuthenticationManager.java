package ru.itis.security;

import ru.itis.models.User;

public interface AuthenticationManager {
    User authenticate(String email, String password);
}
