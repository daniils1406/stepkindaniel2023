package ru.itis.security.filteres;

import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.context.ApplicationContext;
import ru.itis.security.AuthenticationManager;

import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

import static ru.itis.constants.Paths.*;

@WebFilter("/mainPage")
public class SignInFilter extends HttpFilter {

    private AuthenticationManager authenticationManager;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ApplicationContext context = (ApplicationContext) filterConfig.getServletContext().getAttribute("springContext");
        this.authenticationManager = context.getBean(AuthenticationManager.class);
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;


        if (request.getMethod().equals("POST")) { //Проверка на то, что пришли сюда с намерением пойти в пост запрос
            String email = request.getParameter("email");
            String password = request.getParameter("password");

            if (authenticationManager.authenticate(email, password) != null) {
                HttpSession session = request.getSession(true);
                session.setAttribute("authenticated", authenticationManager.authenticate(email, password));
                Optional<String> URL = Arrays.stream(request.getCookies()).filter((str) -> str.getName().equals("URL")).map((str) -> str.getValue()).findAny();
                if (!URL.isEmpty()) {
                    response.sendRedirect(URL.get());
                } else {
                    response.sendRedirect(APPLICATION_PREFIX + MAIN_PAGE);
                }
            } else {
                response.sendRedirect(APPLICATION_PREFIX);
                return;
            }
        } else {
            chain.doFilter(req, res);
        }
    }
}