package ru.itis.security.filteres;

import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.context.ApplicationContext;
import ru.itis.security.AuthenticationManager;

import java.io.IOException;


@WebFilter("/logOut")
public class LogOut extends HttpFilter {

    private AuthenticationManager authenticationManager;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ApplicationContext context = (ApplicationContext) filterConfig.getServletContext().getAttribute("springContext");
        this.authenticationManager = context.getBean(AuthenticationManager.class);
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;


        HttpSession session = request.getSession(false);
        if (session != null) {

            session.setAttribute("authenticated", null);
            response.sendRedirect("/app");
            return;
        } else {
            response.sendRedirect("/app");
            return;
        }

    }
}
