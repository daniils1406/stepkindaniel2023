package ru.itis.security.filteres;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.*;

import java.io.IOException;

import static ru.itis.constants.Paths.*;

@WebFilter("/*")
public class CommonFilter extends HttpFilter {
    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        HttpSession session = request.getSession(false);
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.


        if (request.getRequestURI().equals(APPLICATION_PREFIX + SIGN_IN_USER_PAGE) || request.getRequestURI().equals(APPLICATION_PREFIX + SIGN_UP_USER_PAGE) || request.getRequestURI().equals(APPLICATION_PREFIX + SIGN_UP_USER) || request.getRequestURI().equals(APPLICATION_PREFIX + SIGN_UP_USER_PAGE)) {
            if (session != null) {
                if (session.getAttribute("authenticated") != null) {
                    response.sendRedirect("/app/mainPage");
                    return;
                }
            }
            chain.doFilter(request, response);
            return;
        }


        if (session == null || (session != null && session.getAttribute("authenticated") == null)) {
            Cookie urlCookie = new Cookie("URL", request.getRequestURI());
            urlCookie.setPath("/app");
            response.addCookie(urlCookie);
            response.sendRedirect(APPLICATION_PREFIX + SIGN_IN_USER_PAGE);
            return;
        } else if (session.getAttribute("authenticated") == null) {
            response.sendRedirect(APPLICATION_PREFIX + SIGN_IN_USER_PAGE);
            return;
        }
        chain.doFilter(req, res);
    }

}
