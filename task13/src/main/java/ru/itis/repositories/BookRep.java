package ru.itis.repositories;

import ru.itis.models.Book;

import java.util.List;
import java.util.Optional;

public interface BookRep {
    List<Book> findAll(String column, String desk);

    List<Book> findAllByPages(String from, String to, String column, String desc);

    public Integer findMaxNumberOfPages();

    public List<Book> findAllByNameOrAuthorLike(String argument);

    void save(Book book);

    Optional<Book> findById(Long id);

    void update(Book book);

    void delete(Long id);

    public List<Book> findAllByNameAndPublisher(String name, String publisher);

}
