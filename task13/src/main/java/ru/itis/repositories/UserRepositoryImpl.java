package ru.itis.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import ru.itis.models.User;

import javax.sql.DataSource;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Repository
public class UserRepositoryImpl implements UserRep {

    private static final String SQL_SELECT_BY_EMAIL = "" +
            "select * from book_user where email = :email";

    private static final String SQL_ADD_IMAGE = "" +
            "update book_user set file_name = :fileName WHERE email=:email";

    private static final RowMapper<User> userMapper = (row, rowNumber) -> User.builder()
            .id(row.getLong("id"))
            .email(row.getString("email"))
            .password(row.getString("password"))
            .fileName(row.getString("file_name"))
            .build();

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;


    @Autowired
    public UserRepositoryImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public void save(User user) {
        Map<String, Object> paramsAsMap = new HashMap<>();

        paramsAsMap.put("email", user.getEmail());
        paramsAsMap.put("password", user.getPassword());

        SimpleJdbcInsert insert = new SimpleJdbcInsert(namedParameterJdbcTemplate.getJdbcTemplate());

        Long id = insert.withTableName("book_user")
                .usingGeneratedKeyColumns("id")
                .executeAndReturnKey(new MapSqlParameterSource(paramsAsMap)).longValue();


        user.setId(id);
    }


    @Override
    public Optional<User> findByEmail(String email) {
        try {
            return Optional.ofNullable(namedParameterJdbcTemplate.queryForObject(SQL_SELECT_BY_EMAIL,
                    Collections.singletonMap("email", email),
                    userMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public void addImage(String fileName, String email) {
        try {
            Map<String, String> p = new HashMap<>();
            p.put("fileName", fileName);
            p.put("email", email);
            namedParameterJdbcTemplate.update(SQL_ADD_IMAGE, p);
        } catch (EmptyResultDataAccessException e) {
            throw new RuntimeException(e);
        }
    }

}
