package ru.itis.repositories;

import ru.itis.models.FileInfo;

import java.util.Optional;

public interface FilesRepository {
    Long save(FileInfo fileInfo);

    Optional<FileInfo> findFileNameInStorageById(Long id);

    Optional<FileInfo> findByStorageFileName(String fileName);
}
