package ru.itis.services.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.itis.dto.FileDto;
import ru.itis.models.FileInfo;
import ru.itis.repositories.FilesRepository;
import ru.itis.services.FilesService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

import static ru.itis.dto.FileDto.from;

@Service
public class FilesServiceImpl implements FilesService {

    @Value("${storage.path}")
    private String storagePath;

    private final FilesRepository filesRepository;

    public FilesServiceImpl(FilesRepository filesRepository) {
        this.filesRepository = filesRepository;
    }

    @Override
    public Long upload(FileDto file) {
        String originalFileName = file.getFileName();
        String extension = originalFileName.substring(originalFileName.lastIndexOf("."));
        String storageFileName = UUID.randomUUID() + extension;

        FileInfo fileInfo = FileInfo.builder()
                .description(file.getDescription())
                .size(file.getSize())
                .mimeType(file.getMimeType())
                .originalFileName(originalFileName)
                .storageFileName(storageFileName)
                .build();

        try {
            Files.copy(file.getFileStream(), Paths.get(storagePath + storageFileName));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        return filesRepository.save(fileInfo);
    }

    @Override
    public FileDto getFile(String fileName) {
        FileInfo file = filesRepository.findByStorageFileName(fileName).orElseThrow();
        FileDto fileDto = from(file);
        fileDto.setPath(Paths.get(storagePath + "\\" + fileName));
        return fileDto;
    }

    @Override
    public String getFileNameInStorageById(Long id) {
        FileInfo file = filesRepository.findFileNameInStorageById(id).orElseThrow();
        return file.getStorageFileName();
    }
}