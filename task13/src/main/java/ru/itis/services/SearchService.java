package ru.itis.services;

import ru.itis.dto.BookDto;

import java.util.List;

public interface SearchService {
    List<BookDto> searchBooks(String argument);
}
