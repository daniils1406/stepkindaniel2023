package ru.itis.services;

import ru.itis.dto.FileDto;

public interface FilesService {
    Long upload(FileDto file);

    FileDto getFile(String fileName);

    String getFileNameInStorageById(Long id);
}
