package ru.itis.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

import static ru.itis.constants.Paths.PROFILE_PAGE;

@WebServlet(PROFILE_PAGE)
public class Profile extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String orderColumn = req.getParameter("orderColumn");
        String order = req.getParameter("order");
        Cookie orderCookie = new Cookie("orderColumn", orderColumn);
        Cookie orderCookie1 = new Cookie("order", order);
        orderCookie.setPath("/app");
        orderCookie1.setPath("/app");
        resp.addCookie(orderCookie);
        resp.addCookie(orderCookie1);
        resp.sendRedirect("/app/mainPage");
    }
}
