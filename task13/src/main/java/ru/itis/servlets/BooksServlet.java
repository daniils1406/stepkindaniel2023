package ru.itis.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import ru.itis.dto.BookDto;
import ru.itis.dto.SignUpDto;
import ru.itis.dto.converters.http.HttpFormsConverter;
import ru.itis.services.BookService;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static ru.itis.constants.Paths.*;

@WebServlet(name = "usersServlet", urlPatterns = {USERS_PATH, SIGN_UP_PATH}, loadOnStartup = 1)
public class BooksServlet extends HttpServlet {
    private BookService booksService;
    public ObjectMapper objectMapper;

    @Override
    public void init(ServletConfig config) {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.booksService = context.getBean(BookService.class);
        this.objectMapper = context.getBean(ObjectMapper.class);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getRequestURI().equals(APPLICATION_PREFIX + SIGN_UP_PATH)) {
            SignUpDto signUpData = HttpFormsConverter.from(request);

            booksService.signUp(signUpData);

            response.sendRedirect(APPLICATION_PREFIX);
        } else if (request.getRequestURI().equals(APPLICATION_PREFIX + USERS_PATH)) {
            String body = request.getReader().readLine();
            SignUpDto bookData = objectMapper.readValue(body, SignUpDto.class);
            booksService.signUp(bookData);
            Optional<String> column = Arrays.stream(request.getCookies()).filter((str) -> str.getName().equals("orderColumn")).map((str) -> str.getValue()).findAny();
            Optional<String> order = Arrays.stream(request.getCookies()).filter((str) -> str.getName().equals("order")).map((str) -> str.getValue()).findAny();
            List<BookDto> books;
            if (!column.isEmpty() && !order.isEmpty()) {
                books = booksService.getListOfBooks(column.get(), order.get());
            } else {
                books = booksService.getListOfBooks(null, null);
            }
            String jsonResponse = objectMapper.writeValueAsString(books);
            response.setContentType("application/json");
            response.getWriter().write(jsonResponse);
        } else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }
}
