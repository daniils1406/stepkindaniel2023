package ru.itis.servlets;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import org.springframework.context.ApplicationContext;
import ru.itis.dto.FileDto;
import ru.itis.models.User;
import ru.itis.services.FilesService;
import ru.itis.services.UserService;
import ru.itis.services.impl.UserServiceImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@WebServlet("/avatar")
@MultipartConfig
public class Avatar extends HttpServlet {
    UserService userService;

    FilesService filesService;


    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.userService = context.getBean(UserServiceImpl.class);
        this.filesService = context.getBean(FilesService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User currentUser = (User) req.getSession().getAttribute("authenticated");
        if (currentUser.getFileName() != null) {
            req.setAttribute("fileName", currentUser.getFileName());
        }
        req.getRequestDispatcher("/jsp/userAvatar.jsp").forward(req, resp);
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String description = (new BufferedReader(
                new InputStreamReader(req.getPart("description").getInputStream())).readLine());

        Part filePart = req.getPart("file");
        User user = (User) req.getSession().getAttribute("authenticated");


        FileDto uploadedFileInfo = FileDto.builder()
                .size(filePart.getSize())
                .description(description)
                .mimeType(filePart.getContentType())
                .fileName(filePart.getSubmittedFileName())
                .fileStream(filePart.getInputStream())
                .build();

        Long idOfInsertedImage = filesService.upload(uploadedFileInfo);

        String fileNameInStorage = filesService.getFileNameInStorageById(idOfInsertedImage);

        userService.addImage(fileNameInStorage, user.getEmail());

        user.setFileName(fileNameInStorage);

        resp.sendRedirect("/app/avatar");
    }
}