package ru.itis.servlets;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import ru.itis.dto.UserSignUpDto;
import ru.itis.dto.converters.http.HttpFormsConverter;
import ru.itis.services.UserService;
import ru.itis.services.impl.UserServiceImpl;

import java.io.IOException;

@WebServlet("/signUpUser")
public class UsersServlet extends HttpServlet {

    UserService userService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        userService = context.getBean(UserServiceImpl.class);
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserSignUpDto signUpDto = HttpFormsConverter.formUser(req);
        userService.save(signUpDto);
        resp.sendRedirect("/app");
    }
}
