package ru.itis.servlets;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import ru.itis.dto.FileDto;
import ru.itis.services.FilesService;

import java.io.IOException;
import java.nio.file.Files;

@WebServlet("/images/avatar/*")
public class AvatarServlet extends HttpServlet {

    private FilesService filesService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.filesService = context.getBean(FilesService.class);
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String storageFileName = req.getPathInfo().substring(1);
        FileDto file = filesService.getFile(storageFileName);
        resp.setContentType(file.getMimeType());
        resp.setContentLength(file.getSize().intValue());
        resp.setHeader("Content-Disposition", "filename=\"" + file.getOriginalFileName() + "\"");
        Files.copy(file.getPath(), resp.getOutputStream());
    }
}