drop table if exists book;


create table book
(
    id         bigserial primary key,
    name_of_book        varchar(30),
    author              varchar(30),
    publisher           varchar(30),
    color               varchar(20),
    number_of_pages     INTEGER
);

drop table if exists book_user;

create table book_user
(
    id         bigserial primary key,
    email varchar(30),
    password varchar(30),
    file_name varchar default null
);


drop table if exists file_info;

create table file_info (
                           id bigserial primary key,
                           original_file_name varchar(1000),
                           storage_file_name varchar(100),
                           size bigint,
                           mime_type varchar(50),
                           description varchar(1000)
);