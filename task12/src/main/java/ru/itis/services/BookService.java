package ru.itis.services;

import ru.itis.dto.BookDto;
import ru.itis.dto.FullBookDto;
import ru.itis.dto.SignUpDto;

import java.util.List;

public interface BookService {
    void signUp(SignUpDto signUpDto);
    List<BookDto> getListOfBooks(String column,String desc);

    List<FullBookDto> getListOfBooksByArgument(String from,String to,String column,String desc);

    boolean deleteById(Long id);
}
