package ru.itis.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.dto.BookDto;
import ru.itis.dto.FullBookDto;
import ru.itis.dto.SignUpDto;
import ru.itis.models.Book;
import ru.itis.repositories.BookRep;
import ru.itis.services.BookService;

import java.util.List;

import static ru.itis.dto.BookDto.from;
import static ru.itis.dto.FullBookDto.fromFullBook;

@RequiredArgsConstructor
@Service
public class BooksServiceImpl implements BookService {

    final BookRep bookRep;

    @Override
    public void signUp(SignUpDto signUpDto) {
        Book book = Book.builder()
                .name(signUpDto.getName())
                .author(signUpDto.getAuthor())
                .publisher(signUpDto.getPublisher())
                .color(signUpDto.getColor())
                .numberOfPages(signUpDto.getNumberOfPages())
                .build();
        bookRep.save(book);
    }

    public List<BookDto> getListOfBooks(String column,String desc) {
        return from(bookRep.findAll(column,desc));
    }

    public List<FullBookDto> getListOfBooksByArgument(String from, String to,String column,String desc) {
        return fromFullBook(bookRep.findAllByPages(from, to,column,desc));
    }


    @Override
    public boolean deleteById(Long id) {
        try {
            bookRep.delete(id);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }
}
