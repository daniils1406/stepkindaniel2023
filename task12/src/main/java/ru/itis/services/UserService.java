package ru.itis.services;

import ru.itis.dto.UserDto;
import ru.itis.dto.UserSignUpDto;
import ru.itis.models.User;

public interface UserService {
    void save(UserSignUpDto signUpDto);

}
