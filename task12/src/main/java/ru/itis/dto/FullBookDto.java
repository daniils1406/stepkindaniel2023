package ru.itis.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.models.Book;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FullBookDto {
    private Long id;
    private String name;
    private String author;
    private String publisher;
    private String color;
    private Integer numberOfPages;

    public static FullBookDto fromFullBook(Book book) {
        return FullBookDto.builder()
                .id(book.getId())
                .name(book.getName())
                .author(book.getAuthor())
                .publisher(book.getPublisher())
                .color(book.getColor())
                .numberOfPages(book.getNumberOfPages())
                .build();
    }

    public static List<FullBookDto> fromFullBook(List<Book> books) {
        return books.stream()
                .map(FullBookDto::fromFullBook)
                .collect(Collectors.toList());
    }
}
