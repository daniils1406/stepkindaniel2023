package ru.itis.security;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.itis.models.User;
import ru.itis.repositories.UserRep;

@RequiredArgsConstructor
@Component
public class AuthenticationManagerImpl implements AuthenticationManager {

    private final UserRep userRep;

    @Override
    public boolean authenticate(String email, String password) {
        User user = userRep.findByEmail(email).orElse(null);

        if (user == null) {
            return false;
        }

        return user.getPassword().equals(password);
    }
}
