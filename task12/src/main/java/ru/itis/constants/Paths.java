package ru.itis.constants;

public class Paths {
    public static final String APPLICATION_PREFIX = "/app";

    public static final String SIGN_UP_PATH = "/signUp";
    public static final String SIGN_UP_PATH_TO_HTML = "/signUp.html";
    public static final String USERS_PATH = "/books";

    public static final String SEARCH_PAGE = "/books/search.html";

    public static final String PROFILE_PAGE = "/profile";

    public static final String MAIN_PAGE = "/mainPage";

    public static final String SIGN_IN_USER_PAGE = "/";

    public static final String SIGN_UP_USER_PAGE = "/signUpUser.html";

    public static final String SIGN_UP_USER = "/signUpUser";

    public static final String USERS_SEARCH_PATH = "/books/search";
}
